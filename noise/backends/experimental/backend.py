from noise.backends.default import DefaultNoiseBackend
from noise.backends.experimental.ciphers import AESCCMCipher

class ExperimentalNoiseBackend(DefaultNoiseBackend):
    """
    Contains all the default crypto methods, but also methods not directly endorsed by Noise Protocol specification
    """
    def __init__(self):
        super(ExperimentalNoiseBackend, self).__init__()
        self.ciphers['AESCCM'] = AESCCMCipher

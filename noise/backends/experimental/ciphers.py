import abc

from cryptography.hazmat.primitives.ciphers.aead import AESCCM

from noise.functions.cipher import Cipher
from noise.backends.default.ciphers import CryptographyCipher

class AESCCMCipher(CryptographyCipher):
    @property
    def klass(self):
        return AESCCM

    def format_nonce(self, n):
        return b'\x00\x00\x00\x00' + n.to_bytes(length=8, byteorder='big')
